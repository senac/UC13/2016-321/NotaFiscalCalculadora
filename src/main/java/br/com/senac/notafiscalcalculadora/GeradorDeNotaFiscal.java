
package br.com.senac.notafiscalcalculadora;

import java.util.Calendar;

public class GeradorDeNotaFiscal {
    
    private NFDAO dao ;
    private SAP sap ; 
    private ServicoWeb ws ; 

    public GeradorDeNotaFiscal(NFDAO dao , SAP sap ) {
        this.dao = dao;
        this.sap = sap;
    }
    
    
    
    public NotaFiscal GerarNF(Pedido pedido){
        NotaFiscal nf = new NotaFiscal(
                pedido.getCliente()
              , pedido.getValor() * 0.94, 
                Calendar.getInstance()) ;
        
        this.dao.salvar(nf);
        this.sap.envia(nf);
    //    this.ws.call() ; 
        
        
        
        /// enviar pro SAP 
        
        return nf ; 
        
        
    }
    
    
    
    
    
    
}
