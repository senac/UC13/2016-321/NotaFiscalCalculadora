package br.com.senac.notafiscalcalculadora;

import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

public class GeradorDeNotaFiscalTest {

    @Test
    public void devePersistirNFGerada() throws Exception {

        NFDAO dao = Mockito.mock(NFDAO.class);
        SAP sap = Mockito.mock(SAP.class);

        GeradorDeNotaFiscal geradorDeNotaFiscal = new GeradorDeNotaFiscal(dao, sap);
        Pedido pedido = new Pedido("Jose", 1000, 1);
        NotaFiscal nf = geradorDeNotaFiscal.GerarNF(pedido);
        Mockito.verify(dao).salvar(nf);

    }

    @Test
    public void deveGerarNotaFiscalComDescontoDeSeisPorCento() {

        NFDAO dao = Mockito.mock(NFDAO.class);
        SAP sap = Mockito.mock(SAP.class);

        GeradorDeNotaFiscal geradorDeNotaFiscal = new GeradorDeNotaFiscal(dao, sap);
        Pedido pedido = new Pedido("Jose", 1000, 1);

        NotaFiscal nf = geradorDeNotaFiscal.GerarNF(pedido);

        assertEquals(1000 * 0.94, nf.getValor(), 0.0001);

    }

    @Test
    public void deveEnviarNFGeradaParaoSAP() {
        NFDAO dao = Mockito.mock(NFDAO.class);
        SAP sap = Mockito.mock(SAP.class);

        GeradorDeNotaFiscal geradorDeNotaFiscal = new GeradorDeNotaFiscal(dao, sap);
        Pedido pedido = new Pedido("Jose", 1000, 1);
        NotaFiscal nf = geradorDeNotaFiscal.GerarNF(pedido);
        
        Mockito.verify(sap).envia(nf);

    }

}
